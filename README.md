# SiriTasks

Repository for Apple Developer Academy Nano Challenge 2. Here we show how to use CoreData data storage with SiriKit (which was more complicated than we expected). We implemented INSearchForNotebookItemsIntent here.

We started our challenge with an idea to incorporate SiriKit Support into existing app. While we faced more and more problems with it we decided to create a simple app that stores data with usage of CoreData Framework. Sorry for the UI and possible bugs, but that was not the point of this challenge 😅

First challenge we faced was extracting part of funcionalities from App to the framework, because Siri needed to use it. With much help came this article:
https://medium.com/@yoellev8/sharing-a-core-data-model-with-a-swift-framework-5d191ccec99e

It's important to follow every step. We managed to extract the CoreDataManager to framework that we named CoreDataManagerKit. Yey!

The next challenge we faced was not seeing things created in app. After almost giving app on this challenge this article came with a help:
https://medium.com/@manibatra23/sharing-data-using-core-data-ios-app-and-extension-fb0a176eaee9
It explains how to create optional store shared between app and extension (useful also for app extension challenge). When using this article we needed to make some changes in code created while reading the previuos one.

Finally we came to implementing Intent Handling - we used Apple Documentation for that.

To sum up: SiriKit is messy... if you want to use in your app is better to think about it while starting development. Once again sorry for the possible mistakes in code and shitty UI - this was created in basically one day just to showcase what we learned

STAY LOYAL!

Half The Loyal Ducks 🦆🦆🦆

Adam, Andrea, Florin