//
//  NewTaskViewController.swift
//  SiriTasks
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import CoreDataManagerKit

class NewTaskViewController: UIViewController {

    var rowSelected: Int?
    var name: String?
    var date: Date?
    var status: String?

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var statusTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        datePicker.date = date!
        statusTextField.text = status
        if let taskname = name {
            nameTextField.text = taskname
        } else {
            nameTextField.text = ""
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        if (nameTextField.text?.isEmpty)! {
            
        } else if let rowNumber = rowSelected {
            updateRow(at: rowNumber)
        } else {
            saveTask()
        }
    }

    private func updateRow(at number: Int) {
        CoreDataManager.shared.updateTask(oldname: name!, newName: nameTextField.text!, dueDate: datePicker.date, status: nil)
    }

    private func saveTask() {
        CoreDataManager.shared.createTask(name: nameTextField.text!, dueDate: datePicker.date)
    }
}
