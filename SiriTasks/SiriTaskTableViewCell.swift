//
//  SiriTaskTableViewCell.swift
//  SiriTasks
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit

class SiriTaskTableViewCell: UITableViewCell {

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dueDateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
