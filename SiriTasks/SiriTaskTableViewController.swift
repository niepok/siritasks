//
//  SiriTaskTableViewController.swift
//  SiriTasks
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import CoreDataManagerKit

class SiriTaskTableViewController: UITableViewController {

    private var selectedRow: Int?
    
    @IBAction func addTask(_ sender: Any) {
        performSegue(withIdentifier: "newTask", sender: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        CoreDataManager.shared.fetch()
    }

    override func viewDidAppear(_ animated: Bool) {
        CoreDataManager.shared.fetch()
        tableView.reloadData()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CoreDataManager.shared.tasks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath) as! SiriTaskTableViewCell

        cell.nameLabel.text = CoreDataManager.shared.tasks[indexPath.row].name
        cell.dueDateLabel.text = CoreDataManager.shared.tasks[indexPath.row].dueDate?.convertToString(dateformat: .date)
        cell.statusLabel.text = CoreDataManager.shared.tasks[indexPath.row].status

        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        performSegue(withIdentifier: "editTask", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "newTask" {
            if let destination = segue.destination as? NewTaskViewController {
                destination.date = Date()
                destination.status = "Uncompleted"
            }
        } else if segue.identifier == "editTask" {
            if let destination = segue.destination as? NewTaskViewController,
                let rowSelected = selectedRow {
                destination.rowSelected = rowSelected
                destination.date = CoreDataManager.shared.tasks[rowSelected].dueDate!
                destination.name = CoreDataManager.shared.tasks[rowSelected].name!
                destination.status = CoreDataManager.shared.tasks[rowSelected].status
            }
        }
    }
}
