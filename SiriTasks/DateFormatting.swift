//
//  DateFormatting.swift
//  Sharemind
//
//  Created by Adam Niepokój on 12/12/2018.
//  Copyright © 2018 The Loyal Ducks. All rights reserved.
//

import Foundation

/// Date Format type
enum DateFormatType: String {
    /// Time
    case time = "HH:mm:ss"
    
    case shortTime = "HH:mm"
    
    /// Date with hours
    case dateWithTime = "dd-MMM-yyyy  H:mm"
    
    /// Date
    case date = "dd-MMM-yyyy"
}

extension Date {
    
    func convertToString(dateformat formatType: DateFormatType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatType.rawValue
        let newDate: String = dateFormatter.string(from: self)
        return newDate
    }
    
}
