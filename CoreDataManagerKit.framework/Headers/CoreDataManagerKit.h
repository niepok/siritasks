//
//  CoreDataManagerKit.h
//  CoreDataManagerKit
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CoreDataManagerKit.
FOUNDATION_EXPORT double CoreDataManagerKitVersionNumber;

//! Project version string for CoreDataManagerKit.
FOUNDATION_EXPORT const unsigned char CoreDataManagerKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CoreDataManagerKit/PublicHeader.h>


