//
//  SearchSiriTaskHandler.swift
//  SiriTaskIntentExtension
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import Foundation
import Intents
import CoreDataManagerKit

class SearchSiriTaskHandler: NSObject, INSearchForNotebookItemsIntentHandling {

    // Handling the intent
    func handle(intent: INSearchForNotebookItemsIntent, completion: @escaping (INSearchForNotebookItemsIntentResponse) -> Void) {

        let response: INSearchForNotebookItemsIntentResponse

        let itemType = intent.itemType
        if itemType != .task {
                response = INSearchForNotebookItemsIntentResponse(
                code: .failure,
                userActivity: .none)
            completion(response)
            return
        }

        if CoreDataManager.shared.tasks.isEmpty {
            CoreDataManager.shared.fetch()
        }

        var tasks = CoreDataManager.shared.tasks

        if let dueDateComponents = intent.dateTime?.endDateComponents {
            tasks = tasks.filter {
                $0.dueDate?.compare(to: self.getDate(from: dueDateComponents)!, by: .day) == 1
            }
        }

        var inTasks = [INTask]()
        for task in tasks {
            inTasks.append(INTask(
                title: INSpeakableString(spokenPhrase:  task.name!),
                status: (task.status == "Uncompleted" ? INTaskStatus.notCompleted : INTaskStatus.completed),
                taskType: INTaskType.completable,
                spatialEventTrigger: nil,
                temporalEventTrigger: self.getTemporalEventTrigger(from: task.dueDate!),
                createdDateComponents: self.getDateComponents(from: task.dateOfCreation!),
                modifiedDateComponents: nil,
                identifier: task.name))
        }

        response = INSearchForNotebookItemsIntentResponse(
            code: .success,
            userActivity: .none)
        response.tasks = inTasks
        completion(response)
    }

    // Here we can check if extertnal sources such as APIs are available
    func confirm(intent: INSearchForNotebookItemsIntent, completion: @escaping (INSearchForNotebookItemsIntentResponse) -> Void) {
        completion(INSearchForNotebookItemsIntentResponse(code: .success, userActivity: nil))
    }

    // MARK: Resolving Siri Query

    private func resolveItemType(for intent: INSearchForNotebookItemsIntent,
                         with completion: @escaping (INNotebookItemTypeResolutionResult) -> Void) {
        completion(.success(with: .task))
    }

    // Uncomment this when debbuging on physical device

//    func resolveTitle(for intent: INSearchForNotebookItemsIntent, with completion: @escaping (INSpeakableStringResolutionResult) -> Void) {
//        guard let title = intent.title else {
//            completion(.needsValue())
//            return
//        }
//        let possibleLists = getPossibleLists(for: title)
//        completeResolveListName(with: possibleLists, for: title, with: completion)
//    }

    private func getPossibleLists(for taskName: INSpeakableString) -> [INSpeakableString] {
        var possibleLists = [INSpeakableString]()
        if CoreDataManager.shared.tasks.isEmpty {
            CoreDataManager.shared.fetch()
        }
        for task in CoreDataManager.shared.tasks {
            guard let name = task.name else {
                return []
            }
            if name.lowercased() == taskName.spokenPhrase.lowercased() {
                return [INSpeakableString(spokenPhrase: name)]
            }
            if name.lowercased().contains(taskName.spokenPhrase.lowercased()) || taskName.spokenPhrase.lowercased() == "all" {
                possibleLists.append(INSpeakableString(spokenPhrase: name))
            }
        }
        return possibleLists
    }


    private func completeResolveListName(with possibleLists: [INSpeakableString], for listName: INSpeakableString, with completion: @escaping (INSpeakableStringResolutionResult) -> Void) {
        switch possibleLists.count {
        case 0:
            completion(.unsupported())
        case 1:
            if possibleLists[0].spokenPhrase.lowercased() == listName.spokenPhrase.lowercased() {
                completion(.success(with: possibleLists[0]))
            } else {
                completion(.confirmationRequired(with: possibleLists[0]))
            }
        default:
            completion(.disambiguation(with: possibleLists))
        }
    }

//        func resolveStatus(for intent: INSearchForNotebookItemsIntent, with completion: @escaping (INTaskStatusResolutionResult) -> Void) {
//            let status = intent.status
//
//        }

    func resolveDateSearchType(for intent: INSearchForNotebookItemsIntent, with completion: @escaping (INDateSearchTypeResolutionResult) -> Void) {
        let dateSearchType = intent.dateSearchType
        print("dateSearchType: \(dateSearchType)")
        if dateSearchType == .byDueDate {
            completion(INDateSearchTypeResolutionResult.success(with: dateSearchType))
        } else {
            completion(INDateSearchTypeResolutionResult.notRequired())
        }
    }

    func resolveDateTime(for intent: INSearchForNotebookItemsIntent, with completion: @escaping (INDateComponentsRangeResolutionResult) -> Void) {
        if let dateTime = intent.dateTime {
            print("dateTime: \(dateTime)")
            completion(INDateComponentsRangeResolutionResult.success(with: dateTime))
        } else {
            // TODO: change to disambiguation from set of dates - but how to get them?
            completion(INDateComponentsRangeResolutionResult.disambiguation(with: [INDateComponentsRange(start: nil, end: self.getDateComponents(from: Date()), recurrenceRule: nil)]))
        }
    }

    // MARK: functions for handling data types in intent

    // Due date needs to be set as start date of DateComponentsRange
    private func getTemporalEventTrigger(from date: Date) -> INTemporalEventTrigger {
        return INTemporalEventTrigger(
            dateComponentsRange: INDateComponentsRange(
                start: self.getDateComponents(from: date),
                end: nil
            )
        )
    }

    private func getDateComponents(from date: Date) -> DateComponents {
        let nextTriggerDate = Calendar.current.date(byAdding: .nanosecond, value: 1, to: date)!
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: nextTriggerDate)
        return dateComponents
    }

    private func getDate(from dateComponents: DateComponents) -> Date? {
        return Calendar.current.date(from: dateComponents)
    }
}
