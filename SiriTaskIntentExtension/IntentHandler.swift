//
//  IntentHandler.swift
//  SiriTaskIntentExtension
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import Intents

class IntentHandler: INExtension {

    override func handler(for intent: INIntent) -> Any? {
        if intent is INSearchForNotebookItemsIntent {
            return SearchSiriTaskHandler()
//        } else if intent is INAddTasksIntent {
//            return AddSiriTaskHandler()
//        } else if intent is INSetTaskAttributeIntent {
//            return SetSiriTaskAttributeHandler()
        } else {
            return .none
        }
    }
}
