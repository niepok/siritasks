//
//  AddSiriTaskHandler.swift
//  SiriTaskIntentExtension
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import Foundation
import Intents
import CoreDataManagerKit

class AddSiriTaskHandler: NSObject, INAddTasksIntentHandling {

    func handle(intent: INAddTasksIntent, completion: @escaping (INAddTasksIntentResponse) -> Void) {

        guard let itemNames = intent.taskTitles, itemNames.count > 0 else {
            completion(INAddTasksIntentResponse(code: .failure, userActivity: nil))
            return
        }

        let newTask = INTask(
            title: itemNames[0],
            status: INTaskStatus.notCompleted,
            taskType: .completable,
            spatialEventTrigger: nil,
            temporalEventTrigger: intent.temporalEventTrigger,
            createdDateComponents: self.getDateComponents(from: Date()),
            modifiedDateComponents: nil,
            identifier: itemNames[0].spokenPhrase)

        CoreDataManager.shared.createTask(name: itemNames[0].spokenPhrase, dueDate: (intent.temporalEventTrigger?.dateComponentsRange.endDateComponents?.date)!)

        let response = INAddTasksIntentResponse(code: .success, userActivity: nil)
        response.addedTasks = [newTask]
        completion(response)
    }

    func resolveTaskTitles(for intent: INAddTasksIntent, with completion: @escaping ([INSpeakableStringResolutionResult]) -> Void) {
        guard let title = intent.taskTitles?[0] else {
            completion([INSpeakableStringResolutionResult.needsValue()])
            return
        }
        completion([INSpeakableStringResolutionResult.success(with: title)])
    }

    func resolveTemporalEventTrigger(for intent: INAddTasksIntent, with completion: @escaping (INTemporalEventTriggerResolutionResult) -> Void) {
        guard let temporalEventTrigger = intent.temporalEventTrigger else {
            completion(.needsValue())
            return
        }
        completion(INTemporalEventTriggerResolutionResult.success(with: temporalEventTrigger))
    }

    private func getTemporalEventTrigger(from date: Date) -> INTemporalEventTrigger {
        return INTemporalEventTrigger(
            dateComponentsRange: INDateComponentsRange(
                start: self.getDateComponents(from: date),
                end: nil
            )
        )
    }

    private func getDateComponents(from date: Date) -> DateComponents {
        let nextTriggerDate = Calendar.current.date(byAdding: .nanosecond, value: 1, to: date)!
        let dateComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: nextTriggerDate)
        return dateComponents
    }
}
