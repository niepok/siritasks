//
//  CoreDataManager.swift
//  CoreDataManagerKit
//
//  Created by Adam Niepokój on 11/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import Foundation
import CoreData

public class CoreDataManager {

    public static let shared = CoreDataManager()
    let identifier: String = "niepok.CoreDataManagerKit"   //Your framework bundle ID
    let model: String = "CoreDataModel"                    //Model name

    public var tasks = [Task]()

    lazy var persistentContainer: NSPersistentContainer = {

//        let messageKitBundle = Bundle(identifier: self.identifier)
//        let modelURL = messageKitBundle!.url(forResource: self.model, withExtension: "momd")!
//        let managedObjectModel =  NSManagedObjectModel(contentsOf: modelURL)

        let container = CDMPersistentContainer(name: "CoreDataModel")
        //let container = NSPersistentContainer(name: self.model, managedObjectModel: managedObjectModel!)
        container.loadPersistentStores { (storeDescription, error) in
            if let err = error{
                fatalError("Loading of store failed:\(err)")
            }
        }
        return container
    }()

    public func createTask(name: String, dueDate: Date) {
        let context = persistentContainer.viewContext
        let task = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context) as! Task

        task.name = name
        task.dueDate = dueDate
        task.status = "Uncompleted"
        task.dateOfCreation = Date()

        do {
            try context.save()
            print("Task saved succesfuly")
        } catch let error {
            print(error.localizedDescription)
        }
    }

    public func updateTask(oldname: String, newName: String, dueDate: Date, status: String?) {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Task>(entityName: "Task")
        fetchRequest.predicate = NSPredicate(format: "name = %@", oldname)
        do {
            let fetchedTask = try context.fetch(fetchRequest)[0] as NSManagedObject
            fetchedTask.setValue(newName, forKey: "name")
            fetchedTask.setValue(dueDate, forKey: "dueDate")
            if let newStatus = status {
                fetchedTask.setValue(newStatus, forKey: "status")
            }
            do {
                try context.save()
            } catch {
                print(error)
            }
        } catch {
            print(error)
        }
    }

    public func fetch() {
        let context = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<Task>(entityName: "Task")
        do {
            self.tasks = try context.fetch(fetchRequest)
            for task in tasks {
                print("Task \(task.name) due to: \(task.dueDate)")
            }
        } catch let fetchErr {
            print(fetchErr)
        }
    }
}
