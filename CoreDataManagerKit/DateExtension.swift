//
//  DateExtension.swift
//  CoreDataManagerKit
//
//  Created by Adam Niepokój on 12/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import Foundation

/// Date Format type
public enum DateFormatType: String {
    /// Time
    case time = "HH:mm:ss"

    case shortTime = "HH:mm"

    /// Date with hours
    case dateWithTime = "dd-MMM-yyyy  H:mm"

    /// Date
    case date = "dd-MMM-yyyy"
}

extension Date {

    public func convertToString(dateformat formatType: DateFormatType) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatType.rawValue
        let newDate: String = dateFormatter.string(from: self)
        return newDate
    }

    public func compare(to otherDate: Date, by calendarComponent: Calendar.Component) -> Int {
        let order = Calendar.current.compare(self, to: otherDate, toGranularity: calendarComponent)
        switch order {
        case .orderedSame:
            return 1
        case .orderedAscending:
            return 2
        case .orderedDescending:
            return 3
        default:
            return 4
        }
    }

}
