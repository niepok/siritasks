//
//  CDMPeristentContainer.swift
//  CoreDataManagerKit
//
//  Created by Adam Niepokój on 12/03/2019.
//  Copyright © 2019 Niepok. All rights reserved.
//

import UIKit
import CoreData

class CDMPersistentContainer: NSPersistentContainer {

    override open class func defaultDirectoryURL() -> URL {
        var storeURL = FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: "group.niepok.SiriTasks")
        storeURL = storeURL?.appendingPathComponent("CoreDataModel.sqlite")
        return storeURL!
    }

}
